class Product < ApplicationRecord
  has_many :storage_items
  has_many :availible_in_storages, through: :storage_items, source: :storage

  after_commit :actualize_search_index

  private

  def actualize_search_index
    StorageItem.reindex
  end
end
