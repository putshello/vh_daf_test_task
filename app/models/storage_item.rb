class StorageItem < ApplicationRecord
  belongs_to :storage
  belongs_to :product

  searchkick language: 'russian'

  def search_data
    {
      product_name:        product.name,
      storage_name:        storage.name,
      product_description: product.description,
      vendor_id:           product.vendor_id,
      quantity:            quantity
    }
  end

  # According to specification empty positions should not be shown in search results,
  # so not adding them to index looks better than filtering results
  def should_index?
    quantity > 0
  end

  def self.categories(terms)
    responce = search(terms, aggregations_params)

    responce.aggs['by_name']['buckets'].map do |bucket|
      OpenStruct.new(name:      bucket['key'],
                     quantity:  bucket['total_qantity']['value'].to_i,
                     doc_count: bucket['doc_count'])
    end
  end

  def self.search_in_category(query, category)
    search(query, where: { product_name: category }, includes: %i[product storage]).results
  end

  def self.aggregations_params
    {
      body_options: {
        size: 0,
        aggs: {
          by_name: {
            terms: { field: :product_name },
            aggs: {
              total_qantity: {
                sum: { field: :quantity }
              }
            }
          }
        }
      }
    }
  end
end
