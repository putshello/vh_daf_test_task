// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery/dist/jquery
//= require bootstrap-sass/assets/javascripts/bootstrap-sprockets
//= require rails-ujs
//= require turbolinks
//= require_tree .

$(document).on('ajax:send', function() {
  $('#loader')[0].classList.add('loader')
});

$(document).on('ajax:complete', function() {
  $('#loader')[0].classList.remove('loader')
});

$(document).on('turbolinks:load', function() {
  $('#search-results').on('click', searchInCategory);
});

function searchInCategory(e) {
  const resultsFetched = $(e.target).closest('tr').next('tr').find('.category_results')[0];

  if(resultsFetched) {
    toggleResultsVisibility(e.target);
  }
  else {
    fetchResults(e.target);
  }
}

function toggleResultsVisibility(clickedRow) {
  $(clickedRow).closest('tr').next('tr').toggle();
}

function fetchResults(clickedRow) {
  clickedRow.parentElement.querySelector('a').click();
}
