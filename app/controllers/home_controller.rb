class HomeController < ApplicationController
  def search
    @categories = StorageItem.categories(search_query) if search_query
  end

  def search_in_category
    @results = StorageItem.search_in_category(params[:query], params[:category])
  end

  private

  def search_query
    params.permit(:query)[:query]
  end

  def search_in_category_query
    params.require(:query, :category).permit(:query, :category)
  end
end
