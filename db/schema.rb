# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_180_305_093_053) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'products', force: :cascade do |t|
    t.string 'name'
    t.string 'vendor_id'
    t.text 'description'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['name'], name: 'index_products_on_name'
    t.index ['vendor_id'], name: 'index_products_on_vendor_id'
  end

  create_table 'storage_items', force: :cascade do |t|
    t.bigint 'storage_id'
    t.bigint 'product_id'
    t.integer 'quantity', default: 0, null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['product_id'], name: 'index_storage_items_on_product_id'
    t.index ['storage_id'], name: 'index_storage_items_on_storage_id'
  end

  create_table 'storages', force: :cascade do |t|
    t.string 'name'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['name'], name: 'index_storages_on_name'
  end

  add_foreign_key 'storage_items', 'products'
  add_foreign_key 'storage_items', 'storages'
end
