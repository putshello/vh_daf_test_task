# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

product_names = ['Дифференциал', 'Коробка передач', 'Редуктор моста',
                 'Ретардер',     'Вал коленчатый',  'Бак топливный']

100.times do
  Product.create(
    name: product_names.sample,
    vendor_id: SecureRandom.hex(8),
    description: [Forgery('basic').color, Forgery('basic').text].join(' ')
  )
end

10.times do
  Storage.create(name: Forgery('address').city)
end

1000.times do
  StorageItem.create(
    storage: Storage.order('RANDOM()').first,
    product: Product.order('RANDOM()').first,
    quantity: rand(0..100)
  )
end
