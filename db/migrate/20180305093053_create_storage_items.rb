class CreateStorageItems < ActiveRecord::Migration[5.1]
  def change
    create_table :storage_items do |t|
      t.references :storage, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity, null: false, default: 0

      t.timestamps
    end
  end
end
