class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :vendor_id
      t.text   :description

      t.timestamps
    end
    add_index :products, :name
    add_index :products, :vendor_id
  end
end
