Rails.application.routes.draw do
  root 'home#search'

  get 'search', to: 'home#search'
  get 'search_in_category', to: 'home#search_in_category'
end
