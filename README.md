# README
Ruby version: 2.3.1

System dependencies:
  - nodejs 7.10.1
  - postgreSQL 9.3.21
  - elasticsearch (installed and running)

  for testing
  - google chrome browser
  - chromedriver

Setup instructions:
  - Clone this repo
  - install required gems (run 'bundle install' in command shell)
  - create postgeSQL users vh_daf_dev, vh_daf_test with createdb privileges
  - setup database (rails db:setup)
  - seed test data to database (rails db:seed)
