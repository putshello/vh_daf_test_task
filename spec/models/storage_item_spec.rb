require 'rails_helper'

RSpec.describe StorageItem, type: :model do
  let(:storage)              { FactoryBot.create(:storage, name: 'warehouse') }
  let(:product_for_dogs)     { FactoryBot.create(:product, name: 'bone', description: 'Only for dogs!') }
  let(:available_product)    { FactoryBot.create(:product, description: 'available now!', vendor_id: 'dad76077cb8054ad') }
  let(:not_available_product) { FactoryBot.create(:product, description: 'Not available. Coming soon!') }

  let!(:item_for_dogs)      { FactoryBot.create(:storage_item, product: product_for_dogs, storage: storage, quantity: 500) }
  let!(:available_item)     { FactoryBot.create(:storage_item, product: available_product, quantity: 5) }
  let!(:not_available_item) { FactoryBot.create(:storage_item, product: not_available_product, quantity: 0) }

  describe 'search' do
    it 'by product name' do
      expect(StorageItem.search('bone').results).to eq([item_for_dogs])
    end

    it 'by storage name' do
      expect(StorageItem.search('warehouse').results).to eq([item_for_dogs])
    end

    it 'by product description' do
      expect(StorageItem.search('dog').results).to eq([item_for_dogs])
    end

    it 'by product vendor ID' do
      expect(StorageItem.search('dad76077cb8054ad').results).to eq([available_item])
    end

    it 'can return empty relation' do
      expect(StorageItem.search('cat').results).to eq([])
    end

    it 'return only available items' do
      expect(StorageItem.search('available').results).to     include(available_item)
      expect(StorageItem.search('available').results).to_not include(not_available_item)
    end

    it 'has relevant information after update' do
      expect(StorageItem.search('bone').results).to include(item_for_dogs)

      product_for_dogs.update(name: 'collar')

      expect(StorageItem.search('bone').results).to_not include(item_for_dogs)
      expect(StorageItem.search('collar').results).to   include(item_for_dogs)
    end
  end
end
