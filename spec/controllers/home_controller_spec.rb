require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe '#search' do
    subject { get :search, params: { query: 'lorem ipsum' } }

    it { is_expected.to be_success }
  end
end
