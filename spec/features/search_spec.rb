require 'rails_helper'

RSpec.feature 'Search', type: :feature, js: true do
  given(:searched_product)  { FactoryBot.create(:product, name: 'First', description: 'In results') }
  given(:searched_product2) { FactoryBot.create(:product, name: 'Second', description: 'In results too') }
  given!(:searched_item)    { FactoryBot.create(:storage_item, product: searched_product, quantity: 5) }
  given!(:searched_item2)   { FactoryBot.create(:storage_item, product: searched_product2, quantity: 15) }

  scenario 'by description' do
    visit search_path

    fill_in 'query', with: 'result'
    find('#search-button').click

    expect(page).to have_selector('.category', count: 2)
    expect(page).to have_content(searched_product.name)
    expect(page).to_not have_content(searched_product.description)

    first('.category').click

    expect(page).to_not have_content(searched_product.description)
  end
end
