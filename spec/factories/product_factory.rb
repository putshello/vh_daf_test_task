FactoryBot.define do
  factory :product do
    name        { Forgery('name').company_name }
    vendor_id   { SecureRandom.hex(8) }
    description { "Great #{name} for all purposes" }
  end
end
