FactoryBot.define do
  factory :storage_item do
    product
    storage

    after(:create) do |item, _evaluator|
      item.reindex(refresh: true)
    end
  end
end
