FactoryBot.define do
  factory :storage do
    name { Forgery('address').city }
  end
end
